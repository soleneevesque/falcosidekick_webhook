FROM python:3.6-alpine

# import for python files

COPY requirements.txt requirements.txt
RUN pip3 install --no-cache-dir -r requirements.txt

COPY ./app/ /app/
EXPOSE 5001
CMD ["python3.6", "/app/app.py"]

