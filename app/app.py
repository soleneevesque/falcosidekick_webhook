#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 14 15:16:14 2020

@author: Solene Evesque
"""

from flask import Flask, request, render_template
from elasticsearch import Elasticsearch
from datetime import datetime
import json
import re

# Add unwanted rules to display
rule_filters = ["Set Setuid or Setgid bit", 
                "Falco internal: syscall event drop"]

# Create elasticsearch index



try:
    try:
        with open('app/elasticsearch/es_mapping.json') as json_file:
            mapping = json.load(json_file)
    except BaseException as err:
        print("Can't find mapping file : ", err)
    es = Elasticsearch(['0.0.0.0:9200', "localhost:9200", "elasticsearch-master:9200"],
            sniff_on_connection_fail=True,
            sniffer_timeout=60)
    es.indices.create(index='falco-logs', body=mapping, ignore=400)
    print("Elasticsearch connected")
except BaseException as err:
    print("can't connect to Elasticsearch : ", err)
    exit()

app = Flask(__name__)

# ================================================================ Hook 
@app.route('/hook/', methods=['POST'])
def webhook():
    if request.get_json() != None :
        jsondata = request.get_json()
        rule = find_val_json("rule", jsondata)
        textdata = json.dumps(request.get_json())
        # Ignoring some rules (too many unrelevant events)
        if not any([x in rule for x in rule_filters]):  
            print(jsondata)
            try:
                res = es.index(index="falco-logs", body=jsondata)
                print(res)
                
            except:
                print("error trying to insert data")


    return "End of POST", 200
def find_val_json(key, dictionary):
    return list(find_json(key, dictionary))[0]

def find_json(key, dictionary):
    for k, v in dictionary.items():
        if k == key:
            yield v
        elif isinstance(v, dict):
            for result in find_json(key, v):
                yield result
        elif isinstance(v, list):
            for d in v:
                for result in find_json(key, d):
                    yield result


if __name__ == "__main__":
    app.run(host='0.0.0.0', port='5001')
