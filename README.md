# Falcosidekick webhook server

This project's goal is to deploy a Python3 Flask server in a Kubernetes environment, 
permitting to further filter and sort the Falco output data.


## Script
The image is a python script launching a Flask server on path /hook/
<br/>
Currently, the script ignores some Falco rules contained in the rule_filters list
<br/>
To use with Falcosidekick, you just have to fill the webhook url with :
